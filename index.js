let firstName = "Jose";
console.log("First name: " + firstName);

let lastName = "Miguel";
console.log("Last name: " + lastName);

let age = 36;
console.log("Age: " + age);

let hobbies = "Hobbies:";
console.log(hobbies);

let hobbies1 = ["Basketball", "Video Games", "Guitar"];
console.log(hobbies1);

let workAdd = "Work Address:";
console.log(workAdd);

let fullAdd = {
	houseNumber: 24,
	street: 'Rosary Lane',
	city: 'Las Vegas',
	state: 'Nevada',
}
console.log(fullAdd);

/*Objective 2*/

let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends)

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName1 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName1);

	const lastLocation = "Arctic Ocean";
	firstLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);